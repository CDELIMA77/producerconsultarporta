package br.com.mastertech.producerConsultaPorta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerConsultaPortaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerConsultaPortaApplication.class, args);
	}

	/* No postman só colocar assim
	localhost:8083/acesso/"Emilia Lima"/"8086"
	 */
}
