package br.com.mastertech.producerConsultaPorta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Random;

@RestController
public class AutorizacaoController {

    @Autowired
    private ConsultaPortaProducer consultaPortaProducer;

    @PostMapping("/acesso/{nome}/{porta}")
    public void create(@PathVariable String nome, @PathVariable String porta) {
        Autorizacao autorizacao = new Autorizacao();
        Random random = new Random();
        autorizacao.setCliente(nome);
        autorizacao.setPorta(porta);
        autorizacao.setAutoriza(random.nextBoolean());
        consultaPortaProducer.enviarAoKafka(autorizacao);
    }

}

