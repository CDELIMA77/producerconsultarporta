package br.com.mastertech.producerConsultaPorta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ConsultaPortaProducer {

    @Autowired
    private KafkaTemplate<String, Autorizacao> producer;

    public void enviarAoKafka(Autorizacao autorizacao) {
        producer.send("spec2-cynthia-carvalho-1", autorizacao);
    }

}

