package br.com.mastertech.producerConsultaPorta;

public class Autorizacao {
    private String cliente;
    private String porta;
    private boolean autoriza;

    public Autorizacao() {
    }

    public Autorizacao(String cliente, String porta, boolean autoriza) {
        this.cliente = cliente;
        this.porta = porta;
        this.autoriza = autoriza;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public boolean isAutoriza() {
        return autoriza;
    }

    public void setAutoriza(boolean autoriza) {
        this.autoriza = autoriza;
    }
}


